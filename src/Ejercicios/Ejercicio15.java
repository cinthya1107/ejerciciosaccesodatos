package Ejercicios;

import java.util.Scanner;

/***
 * Escribe un programa que pregunte al usuario si desea analizar calificaciones
 * dealumnos y, s�lo si responde �S� comenzar� el procesamiento de los datos,
 * hasta que elusuario ingrese algo diferente de �S�. Por cada alumno, permitir
 * ingresar su calificaci�n.Si es mayor a 4 el alumno est� aprobado. Finalmente,
 * mostrar �Porcentaje de alumnosaprobados: x %� (donde x es el porcentaje de
 * aprobados sobre el total de calificacionesprocesadas). Tambi�n se debe
 * imprimir �Promedio de los aprobados: y� (donde y es lacalificaci�n promedio,
 * s�lo de los alumnos aprobados).
 * 
 * @param args
 */

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("�Desea analiza calificaciones de los alumnos?, responda 'S' para comenzar");

		double notas;
		double promedio = 0;
		int aprobados = 0;
		int suspensos = 0;
		char analizar = sc.next().charAt(0);

		if (analizar == 'S') {
			while (analizar == 'S') {
				System.out.println("Introduzca una calificaci�n");
				notas = sc.nextDouble();
				if (notas < 4) {
					suspensos++;
				}

				else {
					aprobados++;
					promedio = promedio + notas;
				}

				System.out.println("�Desea introducir mas calificaciones?, pulse 'S' para continuar");
				analizar = sc.next().charAt(0);
			}
		}

		if (aprobados != 0 && suspensos != 0) {
			System.out.println("Porcentaje de aprobados = " + (aprobados * 100) / (aprobados + suspensos) + "%");
			System.out.println("Promedio de aprobados = " + (promedio / aprobados));
		}
	}

}
