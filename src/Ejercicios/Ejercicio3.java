package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que pida una frase e indique la longitud de la misma.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Escribe una frase");
		String frase;
		frase = sc.nextLine();
		System.out.println("La frase: " + frase + ", tiene: " + frase.length() + " caracteres");

	}

}
