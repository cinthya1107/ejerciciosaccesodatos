package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que pida tres n�meros y diga cu�l es mayor y cu�l es el
 * menor.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce tres n�meros");
		int num1, num2, num3;
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		num3 = sc.nextInt();

		if (num1 > num2 && num1 > num3) {
			System.out.println("El mayor es " + num1);
			if (num2 > num3) {
				System.out.println("El numero menor:" + num2);

			} else {
				System.out.println("El n�mero menor: " + num3);
			}
		} else if (num2 > num3) {

			System.out.println("El numero mayor es: " + num2);
			if (num3 < num1) {
				System.out.println("El numero menor: " + num3);
			} else {
				System.out.println("El numero menor es: " + num1);
			}

		} else {

			System.out.println("El numero mayor es:" + num3);
			if (num2 < num1) {
				System.out.println("El numero menor:" + num2);
			} else {
				System.out.println("El numero menor:" + num1);
			}

		}
	}
}
