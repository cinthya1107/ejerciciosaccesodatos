package Ejercicios;

import java.util.Scanner;

/***
 * Con el programa anterior crea un men� que se muestre como el
 * siguiente:1-Sumar.2-Restar.3-Multiplicar.4-Dividir5-SalirEl programa se deje
 * ejecutar en un bucle del que s�lo se podr� salir cuando el usuarioinserte el
 * n�mero 5.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Escribe la operaci�n");
		System.out.println("1-Sumar");
		System.out.println("2-Restar");
		System.out.println("3-Multiplicar");
		System.out.println("4-Dividir");
		System.out.println("5-Salir");
		int operacion = 1;

		while (operacion != 5) {

			int num1, num2;
			System.out.println("Escribe dos n�meros");
			num1 = sc.nextInt();
			num2 = sc.nextInt();
			System.out.println("Escribe la operaci�n");
			operacion = sc.nextInt();

			if (operacion == 1) {
				System.out.println("La suma es: " + (num1 + num2));
			}

			if (operacion == 2) {
				System.out.println("La resta es: " + (num1 + num2));
			}

			if (operacion == 3) {
				System.out.println("La multiplicaci�n es: " + (num1 * num2));
			}

			if (operacion == 4) {
				System.out.println("La divisi�n es: " + (num1 / num2));
			}

		}

	}

}
