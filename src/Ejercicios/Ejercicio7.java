package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que pida al usuario una temperatura en grados Celsius y
 * latransforme en Fahrenheit. Celsius = (5/9) * (Fahrenheit-32)
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce la temperatura");
		double Celsius = sc.nextInt();
		double Fahrenheit = 9 / 5 * Celsius + 32;
		System.out.println("Son " + Fahrenheit);

	}

}
