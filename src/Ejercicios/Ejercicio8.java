package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que reciba una fecha con el formato DD/MM/AAAA y muestre
 * lafrase �Hoy es el d�a DD del mes MM del a�o AAAA�, cambiando las letras por
 * DD, MM yAAAA por lo datos insertados.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int dia, mes, anio;
		System.out.println("Introduce el dia");
		dia = sc.nextInt();

		if (dia > 31 || dia < 1) {
			System.out.println("Inserta un dia valido");
		} else {
			System.out.println("Inserta el mes");
			mes = sc.nextInt();

			if (mes < 1 || mes > 12) {
				System.out.println("Inseta un mes valido");
			} else {
				System.out.println("Inserta el a�o");
				anio = sc.nextInt();

				if (anio > 9999) {
					System.out.println("Inserta un anio valido");
				} else {
					System.out.println("La fecha es:" + dia + "/" + mes + "/" + "/" + anio);

				}
			}
		}
	}

}
