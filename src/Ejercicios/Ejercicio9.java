package Ejercicios;

import java.util.Scanner;

/***
 * Crea un juego que genere un n�mero al azar del 0 al 5 y pida un n�mero al
 * usuariopara que acierte el n�mero.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int numero, dato;
		numero = (int) (Math.random() * 6);

		System.out.println("Inserta un n�mero del 0 al 5");
		dato = sc.nextInt();

		if (numero == dato) {
			System.out.println("Has ganado");
		} else {
			System.out.println("Has perdido. El numero era: " + numero);
		}
	}
}
