package Ejercicios;

import java.util.Scanner;

/***
 * Escribe un programa que pida los precios de las suscripciones que tiene, el
 * programadebe preguntar por los precios hasta que insertemos un cero, y luego
 * muestre el preciomedio y el coste total.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce las subscripciones");
		double total = 0;
		double sus;
		sus = sc.nextDouble();
		int counter = 0;

		while (sus != 0) {
			System.out.println("Introduce las subscripciones");
			total += sus;
			sus = sc.nextDouble();
			counter++;
			double media = total / counter;
			if (sus == 0) {
				System.out.println("El precio total es:  " + total);
				System.out.println("La media es " + media);
			}

		}
	}

}
