package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que reciba una frase y le muestre dada la vuelta (Hola ->
 * aloH);
 * 
 * @author Cinthya
 *
 */

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce una frase");

		String frase = sc.next();

		int i;

		for (i = frase.length(); i > 0; i--) {

			System.out.println(frase.charAt(i - 1));

		}

	}

}
