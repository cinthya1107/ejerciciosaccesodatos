package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que muestre los primeros 10 n�meros de la serie de
 * Fibonacci.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		int a = 0;
		int b = 1;
		int resultado;
		for (int i = 0; i < 10; i++) {
			resultado = a + b;
			System.out.println(a);
			a = b;
			b = resultado;
		}

	}

}
