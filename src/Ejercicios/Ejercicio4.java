package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que pida un n�mero y diga si es par o impar.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce un n�mero");
		int numero;
		numero = sc.nextInt();
		if (numero % 2 == 0) {
			System.out.println("El numero " + numero + " es par");
		} else {
			System.out.println("El numero " + numero + " es impar");
		}

	}
}
