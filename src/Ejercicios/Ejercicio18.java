package Ejercicios;

import java.util.Scanner;

/***
 * Escribe un programa para jugar a piedra, papel y tijera. El programa generar�
 * un n�mero del 0 al 2, de tal manera que el 0 ser� piedra, el 1 ser� papel y
 * el 2 ser� tijera. El programa debe pedir al usuario que inserte su elecci�n y
 * decirle si ha ganado o perdido.
 * 
 * @author Cinthya
 *
 */

public class Ejercicio18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce tu elecci�n");

		int eleccion = sc.nextInt();

		int random;

		random = (int) (Math.random() * 3);

		if (random == 0 && eleccion == 0) {

			System.out.println("Has empatado");

		}

		if (random == 1 && eleccion == 1) {

			System.out.println("Has empatado");

		}

		if (random == 2 && eleccion == 2) {

			System.out.println("Has empatado");

		}

		if (random == 0 && eleccion == 2) {

			System.out.println("Has perdido");

		}

		if (random == 1 && eleccion == 0) {

			System.out.println("Has perdido");

		}

		if (random == 2 && eleccion == 1) {

			System.out.println("Has perdido");

		}

		if (random == 1 && eleccion == 0) {

			System.out.println("Has ganado");

		}

		if (random == 0 && eleccion == 2) {

			System.out.println("Has ganado");

		}

		if (random == 2 && eleccion == 1) {

			System.out.println("Has ganado");

		}

	}

}
