package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que reciba una frase e imprima por pantalla cada letra de la
 * frase.
 * 
 * @author Cinthya
 *
 */

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce una frase");

		String frase = sc.next();

		for (int i = 0; i < frase.length(); i++) {

			System.out.println(frase.charAt(i));

		}

	}

}
