package Ejercicios;

import java.util.Scanner;

/***
 * A�ade al ejercicio 1 la pregunta de qu� tipo de operaci�n quiere que se
 * realice, de talforma que el usuario inserte dos n�meros y la operaci�n que
 * quiere que se ejecute, y el programa devuelva la salida correcta.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		int num1, num2;
		System.out.println("Escribe dos n�meros");
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		System.out.println("Escribe la operaci�n");
		String operacion = sc.next();

		if (operacion.equals("+")) {
			System.out.println("La suma es: " + (num1 + num2));
		}

		if (operacion.equals("-")) {
			System.out.println("La resta es: " + (num1 + num2));
		}

		if (operacion.equals("*")) {
			System.out.println("La multiplicaci�n es: " + (num1 * num2));
		}

		if (operacion.endsWith("/")) {
			System.out.println("La divisi�n es: " + (num1 / num2));
		}

	}

}
