package Ejercicios;

import java.util.Scanner;

/***
 * Con el ejercicio anterior, a�ade tres rondas en las que se ejecute el
 * programa anterior y muestre el resultado que tiene cada ronda al final de la
 * misma y el resultado total.
 * 
 * @author Cinthya
 *
 */

public class Ejercicio19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int counterUsuario = 0;

		int counterMaquina = 0;

		for (int i = 0; i < 3; i++) {

			System.out.println("Introduce tu elecci�n");

			int eleccion = sc.nextInt();

			int random;

			random = (int) (Math.random() * 3);

			if (random == 0 && eleccion == 0) {

				System.out.println("Has empatado");

			}

			if (random == 1 && eleccion == 1) {

				System.out.println("Has empatado");

			}

			if (random == 2 && eleccion == 2) {

				System.out.println("Has empatado");

			}

			if (random == 0 && eleccion == 2) {

				System.out.println("Has perdido");

				counterMaquina++;

			}

			if (random == 1 && eleccion == 0) {

				System.out.println("Has perdido");

				counterMaquina++;

			}

			if (random == 2 && eleccion == 1) {

				System.out.println("Has perdido");

				counterMaquina++;

			}

			if (random == 0 && eleccion == 1) {

				System.out.println("Has ganado");

				counterUsuario++;

			}

			if (random == 2 && eleccion == 0) {

				System.out.println("Has ganado");

				counterUsuario++;

			}

			if (random == 1 && eleccion == 2) {

				System.out.println("Has ganado");

				counterUsuario++;

			}

			if (counterUsuario < counterMaquina) {
				System.out.println("Vas perdiendo " + counterUsuario + "-" + counterMaquina);
			}

			if (counterUsuario > counterMaquina) {
				System.out.println("Vas ganando " + counterUsuario + "-" + counterMaquina);
			}

			if (counterUsuario == counterMaquina) {
				System.out.println("Vas empate " + counterUsuario + "-" + counterMaquina);
			}

		}

	}
}
