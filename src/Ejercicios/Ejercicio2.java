package Ejercicios;

import java.util.Scanner;

/***
 * Crea un programa que pida tres notas por pantalla y saque la media.
 * 
 * @author CinthyaP�rezVazquez
 *
 */

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double nota1, nota2, nota3, media;
		System.out.println("Escribe tres notas");
		nota1 = sc.nextDouble();
		nota2 = sc.nextDouble();
		nota3 = sc.nextDouble();
		media = (nota1 + nota2 + nota3) / 3;
		System.out.println("Esta es la media " + media);

	}

}
